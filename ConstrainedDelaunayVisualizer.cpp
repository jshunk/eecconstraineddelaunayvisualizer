#include "ConstrainedDelaunayVisualizer.h"
#include "Window/Window.h"
#include "Common/Utilities.h"
#include "windows.h"
#include <gl\gl.h>

CConstrainedDelaunayVisualizer::CConstrainedDelaunayVisualizer( const CConstrainedDelaunayTriangulation& krConstrainedDelaunay )
	: CVisible2D()
	, m_krConstrainedDelaunay( krConstrainedDelaunay )
{
}

void CConstrainedDelaunayVisualizer::Draw() const
{
	f32 fWindowHalfWidth = CWindow::GetDefaultWindow()->GetVirtualWidth() / 2;
	f32 fWindowHalfHeight = CWindow::GetDefaultWindow()->GetVirtualHeight() / 2;
	const std::vector< CTriangle2 >& kvTris( m_krConstrainedDelaunay.GetTriangles() );
	const std::vector< CF32Vector2 >& kvPoints( m_krConstrainedDelaunay.GetPoints() );
	const CPolygon2& krPolygon( m_krConstrainedDelaunay.GetPolygon() );
	const std::vector< CPolygon2 >& kvHoles( m_krConstrainedDelaunay.GetHoles() );

	glColor3f( 1.f, 1.f, 1.f );
	glBegin( GL_TRIANGLES );
		for( const CTriangle2& krTri : kvTris )
		{
			f32 fX1 = NUtilities::ConvertCoordinateNoBounds( krTri[ 0 ].X(), -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fX2 = NUtilities::ConvertCoordinateNoBounds( krTri[ 1 ].X(), -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fX3 = NUtilities::ConvertCoordinateNoBounds( krTri[ 2 ].X(), -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fY1 = NUtilities::ConvertCoordinateNoBounds( krTri[ 0 ].Y(), -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
			f32 fY2 = NUtilities::ConvertCoordinateNoBounds( krTri[ 1 ].Y(), -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
			f32 fY3 = NUtilities::ConvertCoordinateNoBounds( krTri[ 2 ].Y(), -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
            glVertex2f( fX1, fY1 );
            glVertex2f( fX2, fY2 );
            glVertex2f( fX3, fY3 );
		}
	glEnd();
	glColor3f( 1.f, 0.f, 1.f );
	for( const CTriangle2& krTri : kvTris )
	{
		glBegin( GL_LINE_LOOP );
			f32 fX1 = NUtilities::ConvertCoordinateNoBounds( krTri[ 0 ].X(), -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fX2 = NUtilities::ConvertCoordinateNoBounds( krTri[ 1 ].X(), -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fX3 = NUtilities::ConvertCoordinateNoBounds( krTri[ 2 ].X(), -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fY1 = NUtilities::ConvertCoordinateNoBounds( krTri[ 0 ].Y(), -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
			f32 fY2 = NUtilities::ConvertCoordinateNoBounds( krTri[ 1 ].Y(), -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
			f32 fY3 = NUtilities::ConvertCoordinateNoBounds( krTri[ 2 ].Y(), -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
			glVertex2f( fX1, fY1 );
			glVertex2f( fX2, fY2 );
			glVertex2f( fX3, fY3 );
		glEnd();
	}
	if (kvTris.size() == 0)
	{
		glColor3f( 1.f, 1.f, 1.f );
		glBegin( GL_LINE_LOOP );
			for (const CF32Vector2& krPoint : krPolygon)
			{
				f32 fX = NUtilities::ConvertCoordinateNoBounds( krPoint.X(), -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
				f32 fY = NUtilities::ConvertCoordinateNoBounds( krPoint.Y(), -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
				glVertex2f( fX, fY );
			}
		glEnd();
		glColor3f( 0.5f, 0.5f, 0.5f );
		for( const CPolygon2& krHole : kvHoles )
		{
			glBegin( GL_LINE_LOOP );
				for (const CF32Vector2& krPoint : krHole)
				{
					f32 fX = NUtilities::ConvertCoordinateNoBounds( krPoint.X(), -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
					f32 fY = NUtilities::ConvertCoordinateNoBounds( krPoint.Y(), -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
					glVertex2f( fX, fY );
				}
			glEnd();
		}
	}
	glColor3f( 1.f, 0.f, 1.f );
	glBegin( GL_LINE_STRIP );
		for( const CF32Vector2& krPoint : kvPoints )
		{
			f32 fX = NUtilities::ConvertCoordinateNoBounds( krPoint.X(), -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fY = NUtilities::ConvertCoordinateNoBounds( krPoint.Y(), -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
            glVertex2f( fX, fY );
		}
	glEnd();
}
