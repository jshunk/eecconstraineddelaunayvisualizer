#pragma once

#include <vector>
#include "Common/Types.h"
#include "Visible2D/Visible2D.h"
#include "Messenger/Listener.h"
#include "Constrained Delaunay Triangulation/ConstrainedDelaunayTriangulation.h"

class CConstrainedDelaunayVisualizer : public CVisible2D
{
public:
			CConstrainedDelaunayVisualizer() = delete;
			CConstrainedDelaunayVisualizer( const CConstrainedDelaunayTriangulation& krConstrainedDelaunay );
	void	Draw() const override;

private:
	const CConstrainedDelaunayTriangulation&	m_krConstrainedDelaunay;
};
